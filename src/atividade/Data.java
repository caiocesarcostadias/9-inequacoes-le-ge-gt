package atividade;

public class Data {

	private byte dia; // {1 .. {28, 29, 30 ou 31} }
	private byte mes; // {1 .. 12}
	private short ano; // {1 .. 9999}

	private boolean ehBissexto(short ano) {
		return (ano % 400 == 0) || ((ano % 4 == 0) && (ano % 100 != 0));
	}

	private byte getUltimoDia(byte mes, short ano) {
		byte ud[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		if (mes == 2 && ehBissexto(ano)) {
			return 29;
		}

		return ud[mes];
	}

	public Data() {
		setAno((byte) 1);
		setMes((byte) 1);
		setDia((byte) 1);
	}

	public Data(byte dia, byte mes, short ano) {
		this();
		setAno(ano);
		setMes(mes);
		setDia(dia);
	}

	public Data(int dia, int mes, int ano) {
		this((byte) dia, (byte) mes, (short) ano);
	}

	public byte getDia() {
		return dia;
	}

	public void setDia(byte dia) {

		byte ultimoDia = getUltimoDia(mes, ano);

		if (dia >= 1 && dia <= ultimoDia) {
			this.dia = dia;
		}
	}

	public byte getMes() {
		return mes;
	}

	public void setMes(byte mes) {
		if (mes >= 1 && mes <= 12) {
			this.mes = mes;
		}
	}

	public short getAno() {
		return ano;
	}

	public void setAno(short ano) {
		if (ano >= 1 && ano <= 9999) {
			this.ano = ano;
		}
	}

	@Override
	public String toString() {
		return getDia() + "/" + getMes() + "/" + getAno();
	}

	public void incrementaDia() {
		setDia((byte) (dia + 1));
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Data other = (Data) obj;
		if (ano != other.ano)
			return false;
		if (dia != other.dia)
			return false;
		if (mes != other.mes)
			return false;
		return true;
	}
	
	public boolean menorOuIgual(Data data) {
		if (this.getAno() <= data.getAno())
			return true;
		if (this.getMes() <= data.getMes())
			return true;
		if (this.getDia() <= data.getDia())
			return true;
		return false;
	}

	public boolean maiorOuIgual(Data data) {
		if (this.getAno() >= data.getAno())
			return true;
		if (this.getMes() >= data.getMes())
			return true;
		if (this.getDia() >= data.getDia())
			return true;
		return false;
	}

	public boolean maior(Data data) {
		if (this.getAno() > data.getAno())
			return true;
		if (this.getMes() > data.getMes())
			return true;
		if (this.getDia() > data.getDia())
			return true;
		return false;
	}
}
