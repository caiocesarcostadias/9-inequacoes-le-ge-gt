package atividade;

public class HorarioNG implements IHorario {

	private int segundos;

	public HorarioNG() {
		segundos = 0;
	}

	public HorarioNG(int hora, int minuto, int segundo) {
		setSegundo((byte) segundo);
		setMinuto((byte) minuto);
		setHora((byte) hora);
	}

	public HorarioNG(HorarioNG horario) {
		this(horario.getHora(), horario.getMinuto(), horario.getSegundo());
	}

	@Override
	public byte getHora() {
		return (byte) converterTempo()[0];
	}

	@Override
	public void setHora(byte hora) {
		if (!(hora >= 0 && hora < 24)) {
			throw new IllegalArgumentException("Hora inválida!");
		}
		segundos = hora * 3600 + (segundos - (getHora() * 3600));
	}

	@Override
	public byte getMinuto() {
		return (byte) converterTempo()[1];
	}

	@Override
	public void setMinuto(byte minuto) {
		if (!(minuto >= 0 && minuto < 60)) {
			throw new IllegalArgumentException("Minuto inválido!");
		}
		segundos = minuto * 60 + (segundos - (getMinuto() * 60));
	}

	@Override
	public byte getSegundo() {
		return (byte) converterTempo()[2];
	}

	@Override
	public void setSegundo(byte segundo) {
		if (!(segundo >= 0 && segundo < 60)) {
			throw new IllegalArgumentException("Segundo inválido!");
		}
		segundos = segundo + (segundos - getSegundo());
	}

	@Override
	public void incrementaHora() {
		if (getHora() == 23)
			segundos -= 3600 * 23;
		else
			segundos += 3600;
	}

	@Override
	public void incrementaMinuto() {
		if (getMinuto() == 59) {
			segundos -= 60 * 59;
			incrementaHora();
		} else
			segundos += 60;
	}

	@Override
	public void incrementaSegundo() {
		if (ehUltimoHorario()) {
			segundos = 0;
		} else
			segundos++;
	}

	@Override
	public void incrementaSegundo(int n) {
		for (int i = 0; i < n; i++) {
			incrementaSegundo();
		}
	}

	@Override
	public boolean ehPrimeiroHorario() {
		return segundos == 0;
	}

	public boolean ehUltimoHorario() {
		return segundos == 86399;
	}

	private int[] converterTempo() {
		int hora = segundos / 3600;
		int minuto = (segundos - 3600 * hora) / 60;
		int segundo = segundos - (3600 * hora) - (60 * minuto);

		int[] horario = { hora, minuto, segundo };

		return horario;
	}

	@Override
	public String toString() {
		return String.format("%02d:%02d:%02d", getHora(), getMinuto(), getSegundo());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HorarioNG other = (HorarioNG) obj;
		if (other.getHora() != other.getHora())
			return false;
		if (other.getMinuto() != other.getMinuto())
			return false;
		if (other.getSegundo() != other.getSegundo())
			return false;
		return true;
	}

	@Override
	public boolean menorOuIgual(IHorario horario) {
		if (this.getHora() <= horario.getHora())
			return true;
		if (this.getMinuto() <= horario.getMinuto())
			return true;
		if (this.getSegundo() <= horario.getSegundo())
			return true;
		return false;
	}

	@Override
	public boolean maiorOuIgual(IHorario horario) {
		if (this.getHora() >= horario.getHora())
			return true;
		if (this.getMinuto() >= horario.getMinuto())
			return true;
		if (this.getSegundo() >= horario.getSegundo())
			return true;
		return false;
	}

	@Override
	public boolean maior(IHorario horario) {
		if (this.getHora() > horario.getHora())
			return true;
		if (this.getMinuto() > horario.getMinuto())
			return true;
		if (this.getSegundo() > horario.getSegundo())
			return true;
		return false;
	}
}
